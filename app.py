from flask import Flask, request
from lib import CashFlowsAndDiscountRates, DiscountRateByTenor, npv

app = Flask(__name__)


@app.get("/npv")
def npv_endpoint():
    data = request.get_json()

    return {
        "npv":
        npv(
            CashFlowsAndDiscountRates(
                data.get("cash_flows", []),
                DiscountRateByTenor(data.get("discounts", 0.0))))
    }
