from dataclasses import dataclass
from typing import Union

@dataclass(frozen=True)
class DiscountRateByTenor():
    discounts: Union[float, list[float], dict[str, float]]

    def discount_rate(self, tenor) -> float:
        discounts = self.discounts
        match type(discounts):
            case single if single is float:
                return discounts
            case listed if listed is list:
                return discounts[tenor] if tenor < len(discounts) else 0.0
            case mapped if mapped is dict:
                return discounts.get(str(tenor), 0.0)


@dataclass(frozen=True)
class CashFlowsAndDiscountRates():
    cash_flows: list[float]
    discount_rates: DiscountRateByTenor


def pv(amount: float, discount_rate: float, period: int) -> float:
    return amount / ((1 + discount_rate)**period)


def npv(cfdr: CashFlowsAndDiscountRates) -> float:
    return sum(
        map(
            lambda periodAmount: pv(
                periodAmount[1],
                cfdr.discount_rates.discount_rate(periodAmount[0]),
                periodAmount[0]), enumerate(cfdr.cash_flows)))
