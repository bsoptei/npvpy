import unittest

from app import app
from flask import Flask, request
from lib import CashFlowsAndDiscountRates, DiscountRateByTenor, npv, pv

TOLERANCE = 1e-5


class NPVTest(unittest.TestCase):

    def compare(self, expected, actual):
        self.assertTrue(abs(expected - actual) <= TOLERANCE)

    def test_discount_rate(self):
        d1 = DiscountRateByTenor(0.04)
        d2 = DiscountRateByTenor([0.03, 0.04, 0.05, 0.06])
        d3 = DiscountRateByTenor({"1": 0.04, "2": 0.05, "3": 0.06})

        for drbt in [d1, d2, d3]:
            self.compare(0.04, drbt.discount_rate(1))

    def test_pv(self):
        self.compare(95.238095, pv(100.0, 0.05, 1))

    def test_npv(self):
        cash_flows = [100.0, -50.0, 35.0]

        input1 = CashFlowsAndDiscountRates(cash_flows,
                                           DiscountRateByTenor(0.05))
        self.compare(84.12698412, npv(input1))

        input2 = CashFlowsAndDiscountRates(
            cash_flows, DiscountRateByTenor([0.05, 0.06, 0.07]))
        self.compare(83.40054416, npv(input2))

        input3 = CashFlowsAndDiscountRates(cash_flows,
                                           DiscountRateByTenor([0.05, 0.06]))
        self.compare(87.83018867, npv(input3))

        input4 = CashFlowsAndDiscountRates(cash_flows, DiscountRateByTenor({}))
        self.compare(85.0, npv(input4))

        input5 = CashFlowsAndDiscountRates(
            cash_flows, DiscountRateByTenor({
                "0": 0.05,
                "1": 0.06,
                "2": 0.07
            }))
        self.compare(83.40054416, npv(input5))


class AppTest(unittest.TestCase):

    def test_npv_endpoint(self):
        with app.test_client() as client:
            response_json = client.get("/npv",
                                       json={
                                           "cash_flows": [10.0, 20.0, 30.0],
                                           "discounts": 0.05
                                       }).get_json()
            self.assertEqual({"npv": 56.25850340136054}, response_json)


if __name__ == '__main__':
    unittest.main()
